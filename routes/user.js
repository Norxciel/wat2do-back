// Express import
const express = require("express");

// Initialize Router
const router = express.Router();

// Base users for testing
const users = [{ name: "Benjamin" }, { name: "Mayvah" }];

router.get("/", (req, res) => {
    console.log("All users called");
    res.json({
        data: [],
        message: "success",
    });
});

router.post("/", (req, res) => {
    console.log(`Create user`);

    res.json({
        data: {},
        message: "success",
    });
});

router
    .route("/:id")
    .get((req, res) => {
        const {
            user,
            params: { id: userID },
        } = req;

        console.log(`Get user with id ${userID}`);
        console.log(`Get user ${user}`);

        res.json({
            data: user,
            message: "success",
        });
    })
    .put((req, res) => {
        const { id: userID } = req.params;

        console.log(`Update user with id ${userID}`);

        res.json({
            data: {},
            message: "success",
        });
    })
    .delete((req, res) => {
        const { id: userID } = req.params;

        console.log(`Delete user with id ${userID}`);

        res.json({
            data: {},
            message: "success",
        });
    });

// Middleware - On Param "id" received
router.param("id", (req, res, next, id) => {
    // Find User and put it in req
    req.user = users[id];
    next();
});

module.exports = router;
