// MomentJS import
const moment = require("moment");
moment.locale("fr"); // Set French locale

// Express import
const express = require("express");

// Initialize app
const app = express();

/**
 * Logger Middleware - Timestamp / Route / Ip
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const logger = (req, res, next) => {
    console.log(
        `${moment().format("YYYY-MM-Do - HH:mm:ss")}> ${
            req.originalUrl
        } called from ${req.ip}`
    );
    next();
};

// Set app to use logger
app.use(logger);

// Default route
app.get("/", (req, res) => {
    res.json({
        message: "success",
    });
});

// Set app to listen or port
app.listen(5000);
